﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using TestCredytyDB.Business.Interfaces;
using TestCredytyDB.Controllers;
using TestCredytyDB.Respository;
using TestCredytyDB.UnitTest.Mocks;

namespace TestCredytyDB.UnitTest
{
    [TestFixture]
    public class RepostitoryManagerTest
    {
        MockDataService mockDataManager;
        MockUserService mockUserService;

        [SetUp]
        public void SetUp()
        {
            mockDataManager  = new MockDataService();
            mockUserService = new MockUserService();
        }

        [TestMethod()]
        public void createUserTest()
        {
            //WHEN
            var mockServiceManager = new Mock<IDeleteUserServiceManager>();
            mockServiceManager.Setup(m => m.DeleteUser(mockDataManager.MockUser().UserId)).Returns(mockDataManager.MockUserResponse);

            var userService = new DeleteUserServiceController();
            var result = userService.DeleteUser(mockDataManager.MockUser().UserId);

            NUnit.Framework.Assert.IsTrue(result.Data != null);
        }

        [TestMethod()]
        public void openConnectionTest()
        {
          
        }

        
    }
}
