﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using TestCredytyDB.Entities;

namespace TestCredytyDB.Respository
{
    public class UserRepository
    {
        /// <summary>
        /// Private var to get the instance to manage the singleton
        /// </summary>
        private static UserRepository instance;

        /// <summary>
        /// Instance to manage the singleton for the class
        /// </summary>
        public static UserRepository Instance
        {
            get
            {
                if (instance != null)
                {
                    return instance;
                }
                instance = new UserRepository();
                return instance;
            }
        }

        /// <summary>
        /// Update user data by specific email
        /// </summary>
        /// <param name="email">Email to validate</param>
        /// <returns>User data</returns>
        public bool DeleteUser(int userId)
        {
            var isDeleted = false;
            var connection = RepositoryManager.Instance.OpenConnection();
            using (connection)
            {                
                using (var cmd = new NpgsqlCommand("fn_delete_user", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", userId);
                    var reader = cmd.ExecuteNonQuery();
                    isDeleted = true;                    
                }
            }

            return isDeleted;
        }
    }
}
