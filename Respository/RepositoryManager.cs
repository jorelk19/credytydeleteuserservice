﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCredytyDB.Entities;

namespace TestCredytyDB.Respository
{
    public class RepositoryManager
    {
        /// <summary>
        /// Private var to get the instance to manage the singleton
        /// </summary>
        private static RepositoryManager instance = null;

        /// <summary>
        /// Instance to manage the singleton for the class
        /// </summary>
        public static RepositoryManager Instance
        {
            get
            {
                if (instance != null)
                {
                    return instance;
                }
                instance = new RepositoryManager();
                return instance;
            }
        }

        /// <summary>
        /// Postgres connection
        /// </summary>
        private NpgsqlConnection PgConnection { get; set; }

        /// <summary>
        /// Method that allow get a connection string
        /// </summary>
        public NpgsqlConnection OpenConnection()
        {
            PgConnection = new NpgsqlConnection();
            var connectionString = DBConnection.PgDbConnection;
            if (!string.IsNullOrWhiteSpace(connectionString))
            {
                try
                {
                    PgConnection = new NpgsqlConnection(connectionString);
                    PgConnection.Open();                    
                }
                catch (NpgsqlException ex)
                {
                    PgConnection.Close();
                    Console.WriteLine(ex.Message);
                }
            }

            return PgConnection;
        }
    }
}
