﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCredytyDB.Entities;

namespace TestCredytyDB.Business.Interfaces
{
    interface IDeleteUserServiceManager
    {        
        /// <summary>
        /// Method used to update the user data by specific email
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>User data</returns>
        UserResponse DeleteUser(int userId);
    }
}
