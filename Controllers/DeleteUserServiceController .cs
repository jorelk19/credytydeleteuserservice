﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestCredytyDB.Business;
using TestCredytyDB.Entities;

namespace TestCredytyDB.Controllers
{
    [Route("api/DeleteUserService")]
    [ApiController]
    public class DeleteUserServiceController : ControllerBase
    {
        /// <summary>
        /// Method used to delete a specific user by id
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>User result</returns>
        [HttpDelete]
        [Route("DeleteUser/{userId}")]
        public UserResponse DeleteUser(int userId)
        {
            return DeleteUserServiceManager.Instance.DeleteUser(userId);
        }

    }
}
